import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

####Database####
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://{}:{}@{}:{}/{}'.format(
    os.getenv("DATABASE_USER"),
    os.getenv("DATABASE_PASSWORD"),
    os.getenv("DATABASE_HOST"),
    os.getenv("DATABASE_PORT"),
    os.getenv("DATABASE_NAME"),
)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
migrate = Migrate(app, db)

db.create_all()