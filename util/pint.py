from quart_openapi import pint


class PintBlueprintModified(pint.BaseRest, pint.Blueprint):
    """Use this instead of :class:`quart_openapi.PintBlueprint` to handle package bug"""

    def __init__(self, *args, **kwargs) -> None:
        """Will forward all arguments and keyword arguments to Blueprints"""
        super().__init__(*args, **kwargs)

    def register(self, app: pint.Pint, options: dict, first_registration: bool = False) -> None:
        """override the base :meth:`~quart_openapi.PintBlueprint.register` method to add the resources to the app registering
        this blueprint, then call the parent register method
        """
        prefix = options.get('url_prefix', '') or self.url_prefix or ''
        app.resources.extend([(res, f'{prefix}{path}', methods) for res, path, methods in self._resources])
        super().register(app, options)