# Flask
from flask_restful import marshal
from sqlalchemy.exc import IntegrityError

# Quart
from quart_openapi import Resource
from quart import request

# Spec
from blueprints import db, app
from util.pint import PintBlueprintModified  # Handle PintBlueprints bug (class modifier)

# Model
from .model import Dosen, RiwayatPendidikan

# 'dosen' penamaan (boleh diganti)
bp_dosen = PintBlueprintModified('dosen', __name__)


@bp_dosen.route("")
class DosenResource(Resource):

    def __init__(self):
        pass

    async def get(self):
        args = {}
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        qry = Dosen.query.get(args["id"])
        riwayat_pendidikan = RiwayatPendidikan.query.filter_by(dosen_id=args['id'])

        riwayat_pendidikan = [marshal(data, RiwayatPendidikan.response_field) for data in riwayat_pendidikan]

        response = marshal(qry, Dosen.response_field)
        response['riwayat_pendidikan'] = riwayat_pendidikan

        if qry and response:
            return {"status":"success", "result":response}, 200, {'Content-Type':'application/json'}

        return {'status':'failed', "result" : "ID Not Found"}, 404, {'Content-Type':'application/json'}

    async def post(self):
        args = await request.get_json()
        keys = [
            "nama",
            "nip",
            "gelar"
        ]
        if not all(key in args for key in keys):
            return {"status":"failed", "result": "Body that required are {}".format(keys)}, 400, {'Content-Type':'application/json'}

        dosen = Dosen(
            args['nama'].title().strip(),
            args['nip'],
            args['gelar'].strip(),
            None,
            None,
            1
        )

        try:
            db.session.add(dosen)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "Dosen Already Exist"}, 400, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}

        app.logger.debug('DEBUG : %s ', dosen)
        return {"status":"success", "result":marshal(dosen, Dosen.response_field)}, 200, {'Content-Type':'application/json'}

    async def put(self):
        args = await request.get_json()
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        keys = [
            "nama",
            "nip",
            "gelar"
        ]
        if not all(key in args for key in keys):
            return {"status":"failed", "result": "Body that required are {}".format(keys)}, 400, {'Content-Type':'application/json'}

        qry = Dosen.query.get(args["id"])

        if qry is None:
            return {'status':'failed',"result":"ID Not Found"}, 404, {'Content-Type':'application/json'}

        # Update data Dosen
        qry.nama = args['nama'].title().strip()
        qry.nip = args['nip']
        qry.gelar = args['gelar'].strip()
        qry.version += 1

        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "NIP Already Exist"}, 400, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}
        return {"status":"success", "result":marshal(qry, Dosen.response_field)}, 200, {'Content-Type':'application/json'}

    async def delete(self):
        args = {}
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        qry = Dosen.query.get(args["id"])
        if qry is None:
            return {'status':'failed',"result":"ID Not Found"}, 404, {'Content-Type':'application/json'}

        try:
            db.session.delete(qry)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "There's still RiwayatPendidikan that has assigned"}, 400, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}
        return {"status":"success", "result":"Data with ID " + args["id"] + " Deleted"}, 200, {'Content-Type':'application/json'}

    def options(self):
        return {}, 200


@bp_dosen.route("/list")
class DosenList(Resource):

    def __init__(self):
        pass

    async def get(self):
        args = {}
        args["page"] = int(request.args.get("page") or 1)
        args["per_page"] = int(request.args.get("per_page") or 25)

        offset = (args['page'] * args['per_page']) - args['per_page']

        qry = Dosen.query

        metadata = {}
        metadata['page'] = args['page']
        metadata['per_page'] = args['per_page']
        result = [marshal(row, Dosen.response_field) for row in qry.limit(args['per_page']).offset(offset).all()]

        return {"status":"success", "metadata":metadata,"result":result}, 200, {'Content-Type':'application/json'}

    def options(self):
        return {}, 200


@bp_dosen.route("/riwayat")
class RiwayatPendidikanResource(Resource):

    def __init__(self):
        pass

    async def get(self):
        args = {}
        args["id"] = request.args.get("id")
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        qry = RiwayatPendidikan.query.get(args['id'])
        if qry:
            return {"status":"success", "result":marshal(qry, RiwayatPendidikan.response_field)}, 200, {'Content-Type':'application/json'}

        return {'status':'failed', "result" : "ID Not Found"}, 404, {'Content-Type':'application/json'}

    async def post(self):
        args = await request.get_json()
        keys = [
            "dosen_id",
            "strata",
            "jurusan",
            "sekolah",
            "tahun_mulai",
            "tahun_selesai",
        ]
        if not all(key in args for key in keys):
            return {"status":"failed", "result": "Body that required are {}".format(keys)}, 400, {'Content-Type':'application/json'}

        riwayat_pendidikan = RiwayatPendidikan(
            args['dosen_id'].strip(),
            args['strata'].strip(),
            args['jurusan'].strip(),
            args['sekolah'].strip(),
            args['tahun_mulai'].strip(),
            args['tahun_selesai'].strip(),
            None,
            None,
            1
        )

        try:
            db.session.add(riwayat_pendidikan)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "RiwayatPendidikan Already Exist"}, 400, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}

        app.logger.debug('DEBUG : %s ', riwayat_pendidikan)
        return {"status":"success", "result":marshal(riwayat_pendidikan, RiwayatPendidikan.response_field)}, 200, {'Content-Type':'application/json'}

    async def put(self):
        args = await request.get_json()
        args["id"] = request.args.get("id")
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        keys = [
            "dosen_id",
            "strata",
            "jurusan",
            "sekolah",
            "tahun_mulai",
            "tahun_selesai",
        ]
        if not all(key in args for key in keys):
            return {"status":"failed", "result": "Body that required are {}".format(keys)}, 400, {'Content-Type':'application/json'}

        qry = RiwayatPendidikan.query.get(args["id"])

        if qry is None:
            return {'status':'failed',"result":"ID Not Found"}, 404, {'Content-Type':'application/json'}

        # Update data Dosen
        qry.dosen_id = args['dosen_id'].strip()
        qry.strata = args['strata'].strip()
        qry.jurusan = args['jurusan'].strip()
        qry.sekolah = args['sekolah'].strip()
        qry.tahun_mulai = args['tahun_mulai'].strip()
        qry.tahun_selesai = args['tahun_selesai'].strip()
        qry.version += 1

        try:
            db.session.commit()
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}
        return {"status":"success", "result":marshal(qry, RiwayatPendidikan.response_field)}, 200, {'Content-Type':'application/json'}

    async def delete(self):
        args = {}
        args["id"] = request.args.get("id")
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        qry = RiwayatPendidikan.query.get(args["id"])
        if qry is None:
            return {'status':'failed',"result":"ID Not Found"}, 404, {'Content-Type':'application/json'}

        try:
            db.session.delete(qry)
            db.session.commit()
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}
        return {"status":"success", "result":"Data with ID " + args["id"] + " Deleted"}, 200, {'Content-Type':'application/json'}

    def options(self):
        return {}, 200


@bp_dosen.route("/riwayat/list")
class RiwayatPendidikanList(Resource):

    def __init__(self):
        pass

    async def get(self):
        args = {}
        args["page"] = int(request.args.get("page") or 1)
        args["per_page"] = int(request.args.get("per_page") or 25)
        args["dosen_id"] = int(request.args.get("dosen_id")) if request.args.get("dosen_id") else None

        offset = (args['page'] * args['per_page']) - args['per_page']

        if args['dosen_id']:
            qry = RiwayatPendidikan.query.filter_by(dosen_id=args['dosen_id'])
        else:
            qry = RiwayatPendidikan.query

        metadata = {}
        metadata['page'] = args['page']
        metadata['per_page'] = args['per_page']
        if args['dosen_id']:
            metadata['dosen_id'] = args['dosen_id']
        result = [marshal(row, RiwayatPendidikan.response_field) for row in qry.limit(args['per_page']).offset(offset).all()]

        return {"status":"success", "metadata":metadata, "result":result}, 200, {'Content-Type':'application/json'}

    def options(self):
        return {}, 200
