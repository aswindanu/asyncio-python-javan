from enum import unique
from blueprints import db
from flask_restful import fields


class Dosen(db.Model):
    __tablename__ = "dosen"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nama = db.Column(db.String(150), nullable=False)
    nip = db.Column(db.Integer, unique=True, nullable=False)
    gelar = db.Column(db.String(100), nullable=False)
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, server_default=db.func.now(), onupdate=db.func.now())
    version = db.Column(db.Integer, nullable=False)

    response_field = {
        'id': fields.Integer,
        'nama': fields.String,
        'nip': fields.Integer,
        'gelar': fields.String,
        'created_at': fields.DateTime,
        'updated_at': fields.DateTime,
        'version': fields.Integer,
    }

    response_field_with_riwayat_pendidikan = {
        'id': fields.Integer,
        'nama': fields.String,
        'nip': fields.Integer,
        'gelar': fields.String,
        'created_at': fields.DateTime,
        'updated_at': fields.DateTime,
        'version': fields.Integer,
        'riwayat_pendidikan': fields.List,
    }

    def __init__(
        self,
        nama,
        nip,
        gelar,
        created_at,
        updated_at,
        version,
    ):
        self.nama = nama
        self.nip = nip
        self.gelar = gelar
        self.created_at = created_at
        self.updated_at = updated_at
        self.version = version

    def __repr__(self):
        return '<dosen %r>' % self.nama


class RiwayatPendidikan(db.Model):
    __tablename__ = "riwayat_pendidikan"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    dosen_id = db.Column(db.Integer, db.ForeignKey('dosen.id'), nullable=False)
    strata = db.Column(db.String(150), nullable=False)
    jurusan = db.Column(db.String(150), nullable=False)
    sekolah = db.Column(db.String(150), nullable=False)
    tahun_mulai = db.Column(db.Integer, nullable=False)
    tahun_selesai = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, server_default=db.func.now(), onupdate=db.func.now())
    version = db.Column(db.Integer, nullable=False)

    response_field = {
        'id': fields.Integer,
        'dosen_id': fields.Integer,
        'strata': fields.String,
        'jurusan': fields.String,
        'sekolah': fields.String,
        'tahun_mulai': fields.Integer,
        'tahun_selesai': fields.Integer,
        'created_at': fields.DateTime,
        'updated_at': fields.DateTime,
        'version': fields.Integer,
    }

    def __init__(
        self,
        dosen_id,
        strata,
        jurusan,
        sekolah,
        tahun_mulai,
        tahun_selesai,
        created_at,
        updated_at,
        version,
    ):
        self.dosen_id = dosen_id
        self.strata = strata
        self.jurusan = jurusan
        self.sekolah = sekolah
        self.tahun_mulai = tahun_mulai
        self.tahun_selesai = tahun_selesai
        self.created_at = created_at
        self.updated_at = updated_at
        self.version = version

    def __repr__(self):
        return '<riwayat_pendidikan %r>' % self.dosen_id