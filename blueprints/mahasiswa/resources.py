# Flask
from flask_restful import marshal
from sqlalchemy.exc import IntegrityError

# Quart
from quart_openapi import Resource
from quart import request

# Spec
from blueprints import db, app
from util.pint import PintBlueprintModified  # Handle PintBlueprints bug (class modifier)

# Model
from .model import Mahasiswa

# 'mahasiswa' penamaan (boleh diganti)
bp_mahasiswa = PintBlueprintModified('mahasiswa', __name__)


@bp_mahasiswa.route("")
class MahasiswaResource(Resource):

    def __init__(self):
        pass

    async def get(self):
        args = {}
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        qry = Mahasiswa.query.get(args["id"])
        if qry:
            return {"status":"success", "result":marshal(qry, Mahasiswa.response_field)}, 200, {'Content-Type':'application/json'}

        return {'status':'failed', "result" : "ID Not Found"}, 404, {'Content-Type':'application/json'}

    async def post(self):
        args = await request.get_json()

        keys = [
            "nama",
            "nim",
            "jenis_kelamin",
            "tempat",
            "tanggal_lahir",
        ]
        if not all(key in args for key in keys):
            return {"status":"failed", "result": "Body that required are {}".format(keys)}, 400, {'Content-Type':'application/json'}

        mahasiswa = Mahasiswa(
            args['nama'].title().strip(),
            args['nim'],
            args['jenis_kelamin'].lower().strip(),
            args['tempat'].title().strip(),
            args['tanggal_lahir'].strip(),
            None,
            None,
            1
        )

        try:
            db.session.add(mahasiswa)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "Mahasiswa Already Exist"}, 400, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}

        app.logger.debug('DEBUG : %s ', mahasiswa)
        return {"status":"success", "result":marshal(mahasiswa, Mahasiswa.response_field)}, 200, {'Content-Type':'application/json'}

    async def put(self):
        args = await request.get_json()
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        keys = [
            "nama",
            "nim",
            "jenis_kelamin",
            "tempat",
            "tanggal_lahir",
        ]
        if not all(key in args for key in keys):
            return {"status":"failed", "result": "Body that required are {}".format(keys)}, 400, {'Content-Type':'application/json'}

        qry = Mahasiswa.query.get(args["id"])

        if qry is None:
            return {'status':'failed',"result":"ID Not Found"}, 404, {'Content-Type':'application/json'}

        # Update data Mahasiswa
        qry.nama = args['nama'].title().strip()
        qry.nim = args['nim']
        qry.jenis_kelamin = args['jenis_kelamin'].lower().strip()
        qry.tempat = args['tempat'].title().strip()
        qry.tanggal_lahir = args['tanggal_lahir'].strip()
        qry.version += 1

        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "NIM Already Exist"}, 400, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}
        return {"status":"success", "result":marshal(qry, Mahasiswa.response_field)}, 200, {'Content-Type':'application/json'}

    async def delete(self):
        args = await request.get_json()
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        qry = Mahasiswa.query.get(args["id"])
        if qry is None:
            return {'status':'failed',"result":"ID Not Found"}, 404, {'Content-Type':'application/json'}

        try:
            db.session.delete(qry)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "There's still Kelas that has assigned"}, 400, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}
        return {"status":"success", "result":"Data with ID " + args["id"] + " Deleted"}, 200, {'Content-Type':'application/json'}

    def options(self):
        return {}, 200


@bp_mahasiswa.route("/list")
class MahasiswaList(Resource):

    def __init__(self):
        pass

    async def get(self):
        args = {}
        args["page"] = int(request.args.get("page") or 1)
        args["per_page"] = int(request.args.get("per_page") or 25)

        offset = (args['page'] * args['per_page']) - args['per_page']

        qry = Mahasiswa.query

        metadata = {}
        metadata['page'] = args['page']
        metadata['per_page'] = args['per_page']
        result = [marshal(row, Mahasiswa.response_field) for row in qry.limit(args['per_page']).offset(offset).all()]

        return {"status":"success", "metadata":metadata, "result":result}, 200, {'Content-Type':'application/json'}

    def options(self):
        return {}, 200
