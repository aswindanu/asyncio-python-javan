from enum import unique
from blueprints import db
from flask_restful import fields


class Mahasiswa(db.Model):
    __tablename__ = "mahasiswa"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nama = db.Column(db.String(300), nullable=False)
    nim = db.Column(db.Integer, unique=True, nullable=False)
    jenis_kelamin = db.Column(db.String(2), nullable=False)
    tempat = db.Column(db.String(500), nullable=False)
    tanggal_lahir = db.Column(db.DateTime, nullable=False)  # laki: 'l', perempuan: 'page'
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, server_default=db.func.now(), onupdate=db.func.now())
    version = db.Column(db.Integer, nullable=False)

    response_field = {
        'id': fields.Integer,
        'nama': fields.String,
        'nim': fields.Integer,
        'jenis_kelamin': fields.String,
        'tempat': fields.String,
        'tanggal_lahir': fields.DateTime,
        'created_at': fields.DateTime,
        'updated_at': fields.DateTime,
        'version': fields.Integer,
    }

    def __init__(
        self,
        nama,
        nim,
        jenis_kelamin,
        tempat,
        tanggal_lahir,
        created_at,
        updated_at,
        version,
    ):
        self.nama = nama
        self.nim = nim
        self.jenis_kelamin = jenis_kelamin
        self.tempat = tempat
        self.tanggal_lahir = tanggal_lahir
        self.created_at = created_at
        self.updated_at = updated_at
        self.version = version

    def __repr__(self):
        return '<mahasiswa %r>' % self.nama