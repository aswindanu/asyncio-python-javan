import os

# Quart
from quart import (
    Quart,
    request,
    json,
    send_from_directory,
)
from quart_openapi import (
    Pint,
)

# CORS
from quart_cors import cors

## database import###
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

## JWT import
from flask_jwt_extended import (
    JWTManager,
    verify_jwt_in_request,
    get_jwt
)
from datetime import timedelta

# wrap
from functools import wraps

# Load ENV Python
import os
from dotenv import load_dotenv
load_dotenv()

# OR, explicitly providing path to '.env'
from pathlib import Path  # python3 only
from json import loads
env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

app = Pint(__name__)
cors(app)

DEBUG = loads(os.getenv("DEBUG").lower())
app.config['APP_DEBUG'] = DEBUG
app.config['PROPAGATE_EXCEPTIONS'] = DEBUG

#################
# JWT
###############

app.config['JWT_SECRET_KEY'] = os.getenv("JWT_SECRET")
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(days=1)

jwt = JWTManager(app)
    
def internal_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt()
        if not claims['status']:
            return {'status':'failed', 'message':'FORBIDDEN | Internal Only'}, 403
        else:
            return fn(*args, **kwargs)
    return wrapper


def non_internal_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt()
        if claims['status']:
            return {'status':'failed', 'message':'FORBIDDEN | JWT NEEDED'}, 403
        else:
            return fn(*args, **kwargs)
    return wrapper

###############################
# Database
###############################

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://{}:{}@{}:{}/{}'.format(
    os.getenv("DATABASE_USER"),
    os.getenv("DATABASE_PASSWORD"),
    os.getenv("DATABASE_HOST"),
    os.getenv("DATABASE_PORT"),
    os.getenv("DATABASE_NAME"),
)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
migrate = Migrate(app, db)

###############################
# Swagger UI
# See docs: https://pypi.org/project/flask-swagger-ui/
###############################

@app.route("/api/swagger.json")
def create_swagger_spec():
    filename = os.path.join(app.root_path, '..', 'asyncio.swagger.json')
    with open(filename) as test_file:
        data = json.load(test_file)
        return data

###############################
# Import blueprints
###############################

from util.swaggerui import bp_ui
from blueprints.dosen.resources import bp_dosen
from blueprints.mahasiswa.resources import bp_mahasiswa
from blueprints.perkuliahan.resources import bp_perkuliahan

# Swagger
app.register_blueprint(bp_ui, url_prefix="/api/docs")

# App
app.register_blueprint(bp_dosen, url_prefix="/dosen")
app.register_blueprint(bp_mahasiswa, url_prefix='/mahasiswa' )
app.register_blueprint(bp_perkuliahan, url_prefix='/perkuliahan' )

db.create_all()