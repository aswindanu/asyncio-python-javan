# Flask
from flask_restful import marshal
from sqlalchemy.exc import IntegrityError

# Quart
from quart_openapi import Resource
from quart import request

# Spec
from blueprints import db, app
from util.pint import PintBlueprintModified  # Handle PintBlueprints bug (class modifier)

# Model
from .model import MataKuliah, DosenMataKuliah, Kelas

# 'perkuliahan' penamaan (boleh diganti)
bp_perkuliahan = PintBlueprintModified('perkuliahan', __name__)


@bp_perkuliahan.route("/mata_kuliah")
class MataKuliahResource(Resource):

    def __init__(self):
        pass

    async def get(self):
        args = {}
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        qry = MataKuliah.query.get(args["id"])
        if qry:
            return {"status":"success", "result":marshal(qry, MataKuliah.response_field)}, 200, {'Content-Type':'application/json'}

        return {'status':'failed', "result" : "ID Not Found"}, 404, {'Content-Type':'application/json'}

    async def post(self):
        args = await request.get_json()
        keys = [
            "mata_kuliah",
            "sks",
        ]
        if not all(key in args for key in keys):
            return {"status":"failed", "result": "Body that required are {}".format(keys)}, 400, {'Content-Type':'application/json'}

        mata_kuliah = MataKuliah(
            args['mata_kuliah'].lower().strip(),
            args['sks'],
            None,
            None,
            1
        )

        try:
            db.session.add(mata_kuliah)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "MataKuliah Already Exist"}, 400, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}

        app.logger.debug('DEBUG : %s ', mata_kuliah)
        return {"status":"success", "result":marshal(mata_kuliah, MataKuliah.response_field)}, 200, {'Content-Type':'application/json'}

    async def put(self):
        args = await request.get_json()
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        keys = [
            "mata_kuliah",
            "sks",
        ]
        if not all(key in args for key in keys):
            return {"status":"failed", "result": "Body that required are {}".format(keys)}, 400, {'Content-Type':'application/json'}

        qry = MataKuliah.query.get(args["id"])

        if qry is None:
            return {'status':'failed',"result":"ID Not Found"}, 404, {'Content-Type':'application/json'}

        # Update data MataKuliah
        qry.mata_kuliah = args['mata_kuliah'].lower().strip()
        qry.sks = args['sks']
        qry.version += 1

        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "Mata Kuliah Already Exist"}, 400, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}
        return {"status":"success", "result":marshal(qry, MataKuliah.response_field)}, 200, {'Content-Type':'application/json'}

    async def delete(self):
        args = {}
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        qry = MataKuliah.query.get(args["id"])
        if qry is None:
            return {'status':'failed',"result":"ID Not Found"}, 404, {'Content-Type':'application/json'}

        try:
            db.session.delete(qry)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "There's still Kelas that has assigned"}, 400, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}
        return {"status":"success", "result":"Data with ID " + args["id"] + " Deleted"}, 200, {'Content-Type':'application/json'}

    def options(self):
        return {}, 200


@bp_perkuliahan.route("/mata_kuliah/list")
class MataKuliahList(Resource):

    def __init__(self):
        pass

    async def get(self):
        args = {}
        args["page"] = int(request.args.get("page") or 1)
        args["per_page"] = int(request.args.get("per_page") or 25)

        offset = (args['page'] * args['per_page']) - args['per_page']

        qry = MataKuliah.query

        metadata = {}
        metadata['page'] = args['page']
        metadata['per_page'] = args['per_page']
        result = [marshal(row, MataKuliah.response_field) for row in qry.limit(args['per_page']).offset(offset).all()]

        return {"status":"success", "metadata":metadata, "result":result}, 200, {'Content-Type':'application/json'}

    def options(self):
        return {}, 200


@bp_perkuliahan.route("/mata_kuliah/dosen")
class DosenMataKuliahResource(Resource):

    def __init__(self):
        pass

    async def get(self):
        args = {}
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        qry = DosenMataKuliah.query.get(args["id"])
        if qry:
            return {"status":"success", "result":marshal(qry, DosenMataKuliah.response_field)}, 200, {'Content-Type':'application/json'}

        return {'status':'failed', "result" : "ID Not Found"}, 404, {'Content-Type':'application/json'}

    async def post(self):
        args = await request.get_json()
        keys = [
            "dosen_id",
            "mata_kuliah_id",
        ]
        if not all(key in args for key in keys):
            return {"status":"failed", "result": "Body that required are {}".format(keys)}, 400, {'Content-Type':'application/json'}

        dosen_mata_kuliah = DosenMataKuliah(
            args['dosen_id'],
            args['mata_kuliah_id'],
            None,
            None,
            1
        )

        try:
            db.session.add(dosen_mata_kuliah)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "DosenMataKuliah Already Exist"}, 400, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}

        app.logger.debug('DEBUG : %s ', dosen_mata_kuliah)
        return {"status":"success", "result":marshal(dosen_mata_kuliah, DosenMataKuliah.response_field)}, 200, {'Content-Type':'application/json'}

    async def put(self):
        args = await request.get_json()
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        keys = [
            "dosen_id",
            "mata_kuliah_id",
        ]
        if not all(key in args for key in keys):
            return {"status":"failed", "result": "Body that required are {}".format(keys)}, 400, {'Content-Type':'application/json'}

        qry = DosenMataKuliah.query.get(args["id"])

        if qry is None:
            return {'status':'failed',"result":"ID Not Found"}, 404, {'Content-Type':'application/json'}

        # Update data DosenMataKuliah
        qry.dosen_id = args['dosen_id']
        qry.mata_kuliah_id = args['mata_kuliah_id']
        qry.version += 1

        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "Dosen/MataKuliah Not Found"}, 500, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}
        return {"status":"success", "result":marshal(qry, DosenMataKuliah.response_field)}, 200, {'Content-Type':'application/json'}

    async def delete(self):
        args = {}
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        qry = DosenMataKuliah.query.get(args["id"])
        if qry is None:
            return {'status':'failed',"result":"ID Not Found"}, 404, {'Content-Type':'application/json'}

        try:
            db.session.delete(qry)
            db.session.commit()
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}
        return {"status":"success", "result":"Data with ID " + args["id"] + " Deleted"}, 200, {'Content-Type':'application/json'}

    def options(self):
        return {}, 200


@bp_perkuliahan.route("/mata_kuliah/dosen/list")
class DosenMataKuliahList(Resource):

    def __init__(self):
        pass

    async def get(self):
        args = {}
        args["page"] = int(request.args.get("page") or 1)
        args["per_page"] = int(request.args.get("per_page") or 25)
        args["dosen_id"] = int(request.args.get("dosen_id")) if request.args.get("dosen_id") else None

        offset = (args['page'] * args['per_page']) - args['per_page']

        # Get query filter by dosen_id, else get all query list
        if args['dosen_id']:
            qry = DosenMataKuliah.query.filter_by(dosen_id=args['dosen_id'])
        else:
            qry = DosenMataKuliah.query

        metadata = {}
        metadata['page'] = args['page']
        metadata['per_page'] = args['per_page']
        if args['dosen_id']:
            metadata['dosen_id'] = args['dosen_id']
        result = [marshal(row, DosenMataKuliah.response_field) for row in qry.limit(args['per_page']).offset(offset).all()]

        return {"status":"success", "metadata":metadata, "result":result}, 200, {'Content-Type':'application/json'}

    def options(self):
        return {}, 200


@bp_perkuliahan.route("/kelas")
class KelasResource(Resource):

    def __init__(self):
        pass

    async def mahasiswa_sks(self, mahasiswa):
        qry_sks = db.session.query(Kelas, MataKuliah).filter_by(mahasiswa_id=mahasiswa).join(MataKuliah, MataKuliah.id == Kelas.mata_kuliah_id).all()
        sks = 0
        if qry_sks:
            for  data in qry_sks:
                sks += data[1].sks
        return sks

    async def get(self):
        args = {}
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        qry = Kelas.query.get(args["id"])
        if qry:
            return {"status":"success", "result":marshal(qry, Kelas.response_field)}, 200, {'Content-Type':'application/json'}

        return {'status':'failed', "result" : "ID Not Found"}, 404, {'Content-Type':'application/json'}

    async def post(self):
        args = await request.get_json()
        keys = [
            "mahasiswa_id",
            "mata_kuliah_id",
            "nama_kelas",
        ]
        if not all(key in args for key in keys):
            return {"status":"failed", "result": "Body that required are {}".format(keys)}, 400, {'Content-Type':'application/json'}

        # SKS filter
        # If total addition > 24, it will be rejected
        qry_matkul = MataKuliah.query.get(args['mata_kuliah_id'])  # get sks of this mata_kuliah

        sks = await self.mahasiswa_sks(args['mahasiswa_id'])  # count total sks mahasiswa
        sks += qry_matkul.sks  # count sks with new additional mata_kuliah
        if sks > 24:
            return {"status":"failed", "result": "SKS more than 24"}, 400, {'Content-Type':'application/json'}

        kelas = Kelas(
            args['mahasiswa_id'],
            args['mata_kuliah_id'],
            args['nama_kelas'].upper().strip(),
            None,
            None,
            1
        )

        try:
            db.session.add(kelas)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {"status":"failed", "result": "Kelas Already Exist"}, 400, {'Content-Type':'application/json'}
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}

        app.logger.debug('DEBUG : %s ', kelas)
        return {"status":"success", "result":marshal(kelas, Kelas.response_field)}, 200, {'Content-Type':'application/json'}

    async def put(self):
        args = await request.get_json()
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        keys = [
            "mahasiswa_id",
            "mata_kuliah_id",
            "nama_kelas",
        ]
        if not all(key in args for key in keys):
            return {"status":"failed", "result": "Body that required are {}".format(keys)}, 400, {'Content-Type':'application/json'}

        qry = Kelas.query.get(args["id"])

        if qry is None:
            return {'status':'failed',"result":"ID Not Found"}, 404, {'Content-Type':'application/json'}

        # SKS filter
        # If total update > 24, it will be rejected
        if args['mata_kuliah_id']:
            qry_oldmatkul = MataKuliah.query.get(qry.mata_kuliah_id)  # get sks of old mata_kuliah
            qry_newmatkul = MataKuliah.query.get(args['mata_kuliah_id'])  # get sks of new mata_kuliah

            sks = await self.mahasiswa_sks(args['mahasiswa_id'])  # count total sks mahasiswa
            sks -= qry_oldmatkul.sks  # remove old sks mata_kuliah
            sks += qry_newmatkul.sks  # count sks with new aditional mata_kuliah
            if sks > 24:
                return {"status":"failed", "result": "SKS more than 24"}, 400, {'Content-Type':'application/json'}

        # Update data Kelas
        qry.mahasiswa_id = args['mahasiswa_id']
        qry.mata_kuliah_id = args['mata_kuliah_id']
        qry.nama_kelas = args['nama_kelas'].upper().strip()
        qry.version += 1

        try:
            db.session.commit()
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}
        return {"status":"success", "result":marshal(qry, Kelas.response_field)}, 200, {'Content-Type':'application/json'}

    async def delete(self):
        args = {}
        args["id"] = int(request.args.get("id"))
        if not args["id"]:
            return {"status":"failed", "result": "Query param that required are '{}'".format("id")}, 400, {'Content-Type':'application/json'}

        qry = Kelas.query.get(args["id"])
        if qry is None:
            return {'status':'failed',"result":"ID Not Found"}, 404, {'Content-Type':'application/json'}

        try:
            db.session.delete(qry)
            db.session.commit()
        except Exception:
            db.session.rollback()
            return {"status":"failed", "result": "Internal Server Error"}, 500, {'Content-Type':'application/json'}
        return {"status":"success", "result":"Data with ID " + args["id"] + " Deleted"}, 200, {'Content-Type':'application/json'}

    def options(self):
        return {}, 200


@bp_perkuliahan.route("/kelas/list")
class KelasList(Resource):

    def __init__(self):
        pass

    async def get(self):
        args = {}
        args["page"] = int(request.args.get("page") or 1)
        args["per_page"] = int(request.args.get("per_page") or 25)
        args["mahasiswa_id"] = int(request.args.get("mahasiswa_id")) if request.args.get("mahasiswa_id") else None

        offset = (args['page'] * args['per_page']) - args['per_page']

        if args['mahasiswa_id']:
            qry = Kelas.query.filter_by(mahasiswa_id=args['mahasiswa_id'])
        else:
            qry = Kelas.query

        metadata = {}
        metadata['page'] = args['page']
        metadata['per_page'] = args['per_page']
        if args['mahasiswa_id']:
            metadata['mahasiswa_id'] = args['mahasiswa_id']
        result = [marshal(row, Kelas.response_field) for row in qry.limit(args['per_page']).offset(offset).all()]

        return {"status":"success", "metadata":metadata, "result":result}, 200, {'Content-Type':'application/json'}

    def options(self):
        return {}, 200
