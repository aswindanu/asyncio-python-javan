from enum import unique
from blueprints import db
from flask_restful import fields


class MataKuliah(db.Model):
    __tablename__ = "mata_kuliah"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    mata_kuliah = db.Column(db.String(150), unique=True, nullable=False)
    sks = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, server_default=db.func.now(), onupdate=db.func.now())
    version = db.Column(db.Integer, nullable=False)

    response_field = {
        'id': fields.Integer,
        'mata_kuliah': fields.String,
        'sks': fields.Integer,
        'created_at': fields.DateTime,
        'updated_at': fields.DateTime,
        'version': fields.Integer,
    }

    def __init__(
        self,
        mata_kuliah,
        sks,
        created_at,
        updated_at,
        version,
    ):
        self.mata_kuliah = mata_kuliah
        self.sks = sks
        self.created_at = created_at
        self.updated_at = updated_at
        self.version = version

    def __repr__(self):
        return '<mata_kuliah %r>' % self.mata_kuliah


class DosenMataKuliah(db.Model):
    __tablename__ = "dosen_mata_kuliah"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    dosen_id = db.Column(db.Integer, db.ForeignKey('dosen.id'), nullable=False)
    mata_kuliah_id = db.Column(db.Integer, db.ForeignKey('mata_kuliah.id'), nullable=False)
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, server_default=db.func.now(), onupdate=db.func.now())
    version = db.Column(db.Integer, nullable=False)

    response_field = {
        'id': fields.Integer,
        'dosen_id': fields.Integer,
        'mata_kuliah_id': fields.Integer,
        'created_at': fields.DateTime,
        'updated_at': fields.DateTime,
        'version': fields.Integer,
    }

    def __init__(
        self,
        dosen_id,
        mata_kuliah_id,
        created_at,
        updated_at,
        version,
    ):
        self.dosen_id = dosen_id
        self.mata_kuliah_id = mata_kuliah_id
        self.created_at = created_at
        self.updated_at = updated_at
        self.version = version

    def __repr__(self):
        return '<dosen_mata_kuliah %r>' % self.id


class Kelas(db.Model):
    __tablename__ = "kelas"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    mahasiswa_id = db.Column(db.Integer, db.ForeignKey('mahasiswa.id'), nullable=False)
    mata_kuliah_id = db.Column(db.Integer, db.ForeignKey('mata_kuliah.id'), nullable=False)
    nama_kelas = db.Column(db.String(100), nullable=False)
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, server_default=db.func.now(), onupdate=db.func.now())
    version = db.Column(db.Integer, nullable=False)

    response_field = {
        'id': fields.Integer,
        'mahasiswa_id': fields.Integer,
        'mata_kuliah_id': fields.Integer,
        'nama_kelas': fields.String,
        'created_at': fields.DateTime,
        'updated_at': fields.DateTime,
        'version': fields.Integer,
    }

    def __init__(
        self,
        mahasiswa_id,
        mata_kuliah_id,
        nama_kelas,
        created_at,
        updated_at,
        version,
    ):
        self.mahasiswa_id = mahasiswa_id
        self.mata_kuliah_id = mata_kuliah_id
        self.nama_kelas = nama_kelas
        self.created_at = created_at
        self.updated_at = updated_at
        self.version = version

    def __repr__(self):
        return '<kelas %r>' % self.nama_kelas