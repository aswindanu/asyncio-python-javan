# Asyncio Python Task (by JAVAN)

## Documentation

- Tugas CRUD Python : https://docs.google.com/document/d/1Kii_CtTY_pOQ6WeqShzWyP4_n-FR6xRv-nr-NnSGZ-Y/edit?usp=sharing

- ERD : https://lucid.app/lucidchart/071e25ee-33a0-437a-8cd9-c973706ea06f/edit?viewport_loc=-139%2C-13%2C2164%2C1036%2C0_0&invitationId=inv_324b1157-3ea6-46e8-a18e-f27e6df3f1be

- Answer Documentation : https://docs.google.com/document/d/1H-qQhfhGnVD6S0aPzL8um1RUbhuzbblG0ptuhbEJkNM/edit?usp=sharing

- Gitlab repo : https://gitlab.com/aswindanu/asyncio-python-javan


## Prequisitions

What need to be installed in this project are:

    python
    pip
    mysql
    virtualenv
    docker (optional)

Technology that used for development are:

- Programming Language : `Python 3.7.2`
- Framework : `Flask v2.0.1`
- Database : `MySQL`
- Documentation API : `Swagger/postman`


## This project using this kind of library:

* API Framework : 
    Quart : https://pgjones.gitlab.io/quart/index.html
    Flask : https://www.fullstackpython.com/flask.html
* ORM : https://flask-sqlalchemy.palletsprojects.com/en/2.x/
* Configs : https://pypi.org/project/python-dotenv/
* Unit Test : https://docs.pytest.org/en/latest/
* Logging : https://docs.python.org/2/library/logging.html


#### How to Start Project

1. Clone this project:
    ```
    git clone https://gitlab.com/aswindanu/asyncio-python-javan asyncio
    ```

2. Create env:
    ```
    python -m venv env
    ```

3. Activate ENV (Linux/OSX):
    ```
    source env/bin/activate
    ```

4. Go to dir project:
    ```
    cd asyncio
    ```

4. install requirements:
    ```
    pip install -r requirements.txt
    ```

5. Create .env in root project:
    ```
    touch .env
    ```

6. configure env in .env file

    - DEBUG=True
    - JWT_SECRET=`your-jwt-secret`
    - DATABASE_USER=`username`
    - DATABASE_PASSWORD=`password`
    - DATABASE_HOST=127.0.0.1
    - DATABASE_PORT=3306
    - DATABASE_NAME=`database-name`

7. initiate the database:
    ```
    export FLASK_APP=util/db
    flask db init
    ```

8. migrate the database:
    ```
    flask db migrate
    ```

9. upgrade database:
    ```
    flask db upgrade
    ```

10. run the program (using Quart ASGI):
    
    ```
    export QUART_APP=app:app
    quart run
    ```

    or just run sheel script command
    ```
    run.sh
    ```

## Swagger collection
Collection & environment of Swagger can be accessed from this listed endpoint

    https://app.swaggerhub.com/apis-docs/aswindanu/asyncio/1.0


## Postman collection
Collection & environment of Postman can be imported from this listed file

    asyncio.postman_collection
    asyncio.postman_environment


#### Run with Docker Compose
```docker-compose up```


#### Update requirements (virtualenv only)
```pip freeze -l > requirements.txt```
