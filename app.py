import logging, sys
from logging.handlers import RotatingFileHandler


from blueprints import (
    app,
    migrate,
    DEBUG
)

from cachelib import SimpleCache


cache = SimpleCache()

######################################


if __name__ == '__main__':
    formatter = logging.Formatter("[%(asctime)s] {%(pathname)s:%(lineno)s} %(levelname)s - %(message)s")
    log_handler = RotatingFileHandler("%s/%s" %(app.root_path, '../storage/log/app.log'), maxBytes=1000000, backupCount=10)
    log_handler.setLevel(logging.INFO)
    log_handler.setFormatter(formatter)
    app.logger.addHandler(log_handler)

    try:
        if sys.argv[1] == 'db':
            migrate.run()
            sys.exit()
        else:
            app.run(debug=DEBUG, host = '0.0.0.0', port=5000)
    except IndexError as e:
        # define log format and create a rotating log with max size of 1mb and max backup to 10 files
        app.run(debug=DEBUG, host='0.0.0.0', port=5000)

